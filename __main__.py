from project.cmdargs import get_args

def main(inputfiles, out_dir, silent=False):
    return

def console():
    args = get_args()
    main(args.files, args.output_dir)

if __name__ == '__main__':
    console()

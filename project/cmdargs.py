import argparse

def get_args():
    parser = argparse.ArgumentParser(description=__doc__.strip())
    parser.add_argument('-V', '--version', action='version',
                        version=__version__)
    parser.add_argument('files', type=str, nargs='+',
                        help='input filenames')
    parser.add_argument('-o', '--output-dir', default='out', metavar='DIR',
                        help='output directory.  default "./out/"')
    parser.add_argument('--silent', action='store_true',
                        help='do not print')
    return parser.parse_args()

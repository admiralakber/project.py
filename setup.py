#!/usr/bin/env python3
"""
Metadata and setup info for installation.
See  https://packaging.python.org/en/latest/distributing.html
"""

import setuptools


__version__ = '0.0.0'


with open('README.org') as f:
    long_description = f.read()

with open('requirements.txt') as f:
    install_requires = f.read().splitlines()
    
    
config = dict(
    name='PYTHON PROJECT',
    version=__version__,
    description='PYTHON PROJECT DESCRIPTION',
    long_description=long_description,

    author='Aqeel Akber',
    author_email='aqeel.akber@gmail.com',
    license='Unlicence',
    url='https://gitlab.com/admiralakber/project.py',

    keywords='project',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: The Unlicence',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Scientific/Engineering',
    ],

    packages=['.'],
    install_requires=install_requires,
    extras_require={},
    entry_points={'console_scripts': [
        'project=__main__:console', 'project-helper=someother:console']},
)

if __name__ == '__main__':
    setuptools.setup(**config)

# Sample unit test file.
# Ordinarily you'll just import the unit the following snippet
# allows importing units within the project
import sys, os
sys.path.append(os.path.dirname(__file__) + "../project")

# For example, have your function
def inc(x):
    return x + 1

# Write a void() function and assert an answer.
def test_answer():
    assert inc(3) == 5
